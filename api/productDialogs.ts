import { reactive, readonly } from "vue";
import ProductModel from "~/types/product.model";

const editProductModal = reactive({
    isActive:false,
    productData: {},
    productId: 0
});
const deleteProductModal = reactive({
    isActive:false,
    productId: 0
});

export default function productDialogs() {

    const toggleEditModal = (productId:number,productData:ProductModel) => {
        editProductModal.isActive = !editProductModal.isActive;
        editProductModal.productData = productData;
        editProductModal.productId = productId;
    };
    const toggleDeleteModal = (productId:number) => {
        deleteProductModal.isActive = !deleteProductModal.isActive;
        deleteProductModal.productId = productId;
    };

    return {
        editProductModal: editProductModal,
        toggleEditModal,
        toggleDeleteModal,
        deleteProductModal:deleteProductModal
    };
}
