import {reactive} from "vue";

const snackbarModal = reactive({
    isActive: false
});

export default function snackbar() {
    const toggleSnackbar = () => {
        snackbarModal.isActive = !snackbarModal.isActive
    }
    return {
        toggleSnackbar,
        snackbarModal
    }
}
