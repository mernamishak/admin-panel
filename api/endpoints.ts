import axios, {AxiosResponse} from 'axios';
import ProductModel from "~/types/product.model";
import AuthModel from "~/types/auth.model";

export default (axios: any, baseURL: string) => ({
    async getProducts(): Promise<AxiosResponse> {
        try {
            const response = await axios.get(`${baseURL}/products`)
            return response as AxiosResponse<any>;
        } catch (error) {
            throw error
        }
    },
    async getProductsWithLimit(): Promise<AxiosResponse> {
        try {
            const response = await axios.get(`${baseURL}/products?limit=5`)
            return response as AxiosResponse<any>;
        } catch (error) {
            throw error
        }
    },
    async updateProducts(productId: number, productBody: ProductModel): Promise<AxiosResponse> {
        try {
            const response = await axios.put(`${baseURL}/products/${productId}`, {
                productBody
            })

            return response as AxiosResponse<any>;
        } catch (error) {
            throw error
        }
    },
    async deleteProducts(productId: number): Promise<AxiosResponse> {
        try {
            const response = await axios.delete(`${baseURL}/products/${productId}`)

            return response as AxiosResponse<any>;
        } catch (error) {
            throw error
        }
    },
    async addNewProduct(productBody: ProductModel): Promise<AxiosResponse> {
        try {
            const response = await axios.post(`${baseURL}/products`, {
                productBody
            })

            return response as AxiosResponse<any>;
        } catch (error) {
            throw error
        }
    },
    async getCategories(): Promise<AxiosResponse> {
        try {
            const response = await axios.get(`${baseURL}/products/categories`)
            return response as AxiosResponse<any>;
        } catch (error) {
            throw error
        }
    },
    async login(loginBody: AuthModel) {
        try {
            const response = await axios.post(`${baseURL}/auth/login`, {
                username: loginBody.username,
                password: loginBody.password
            })
            return response as AxiosResponse<any>;
        } catch (error) {
            return  error;
        }
    },
    async getUserDetails() {
        try {
            const response = await axios.get(`${baseURL}/users/1`)
            return response as AxiosResponse<any>;
        } catch (error) {
            throw  error;
        }
    }
});
