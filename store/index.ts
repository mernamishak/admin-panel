import {createStore} from "vuex";

export default createStore({
    state: {
        products: [],
        updatedProduct: null,
        updatedProductList: [],
        categoryList:[],
        updatedCategoryList:[]
    },

    mutations: {
        setProducts(state: any, products: []) {
            state.products = products;
        },
        updateProduct(state: any, updatedProduct: any) {
            state.updatedProductList = state.products.map((product: any) => {
                if (product.id == updatedProduct.id) {
                    return updatedProduct;
                }

                return product;
            });

        },
        deleteProduct(state: any, deletedProduct: any) {
            state.updatedProductList = state.updatedProductList.filter((product: any) => {
                return product.id != deletedProduct.id;
            });
        },
        addProduct(state: any, newProduct: any) {
            const productExist = state.updatedProductList.find((product: any) => product.id == newProduct.id);
            if (!productExist) {
                state.updatedProductList.push(newProduct);
            }

        },
        newProductList(state: any, productList: []) {
            state.updatedProductList = productList
        },
        setCategories(state: any, categories: []) {
            state.categoryList = categories;
        },
        newCategoryList(state: any, categoryList: []) {
            state.updatedCategoryList = categoryList
        },
        addCategory(state: any, newCategory: string) {
            const categoryExist = state.categoryList.find((category: string) => category?.includes(newCategory));
            if (!categoryExist) {
                state.updatedCategoryList.push(newCategory);
            }
        }
    }
})
