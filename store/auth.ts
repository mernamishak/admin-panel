import {defineStore} from 'pinia'
import {createStore} from "vuex";
import axios, {AxiosResponse} from "axios";
import AuthModel from "~/types/auth.model";


export default createStore({
    state: {
        authenticateUser: false,
        authenticateUserInfo: {},
        authResponse: false
    },
    getters: {
        authenticateUser(state) {
            return state.authenticateUser
        },
        authenticateUserInfo(state) {
            return state.authenticateUserInfo
        },
        authResponse(state) {
            return state.authResponse
        },

    },
    mutations: {
        async auth(state, loginBody: AuthModel) {
            const token = useCookie('token');
            const {$api} = useNuxtApp();
            await $api.login(loginBody).then(async (response: any) => {
                if (response?.data?.token) {
                    token.value = response?.data?.token;
                    state.authenticateUser = true;
                    await $api.getUserDetails().then((response: any) => {
                        if (response?.data) {
                            state.authenticateUserInfo = response?.data
                            if (process.client) {
                                localStorage.setItem('UserInfo', JSON.stringify(response?.data))

                            }
                        }
                    })
                } else {
                    state.authResponse = response?.response?.data
                }
            })
        },
        toggleAuthenticateUser(state, userInfo) {
            state.authenticateUser = true;
            state.authenticateUserInfo = userInfo
        }

    }
})
