interface ProductModel {
    title: string,
    price: number,
    description: string,
    image: string,
    category: string
}
export default ProductModel;
