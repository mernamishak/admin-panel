import Auth from "~/store/auth";

export default defineNuxtRouteMiddleware((to, from) => {
    const auth = Auth;
    const token = useCookie('token');
    let isAuthenticatedUser = false;
    let userInfo = null
    if (token && token?.value !== undefined ) {
        if(process.client) {
            userInfo  = JSON.parse(localStorage.getItem('UserInfo'));
        }
        auth.commit('toggleAuthenticateUser', userInfo)

    } else if (to.path !== '/login'){
        if(process.client) {
            localStorage.removeItem('UserInfo')
        }
        return navigateTo('/login')
    }

})
