import { $fetch, FetchOptions } from 'ofetch';
import Endpoints from "../api/endpoints";

export default defineNuxtPlugin((nuxtApp) => {
     const { $axios}= useNuxtApp();
     const API_BASE_URL= '/api'
     const endpoints = Endpoints($axios,API_BASE_URL);

    return {
        provide: {
            api: endpoints
        }
    };
});
