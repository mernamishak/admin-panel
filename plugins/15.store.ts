import store from "../store";
import {useStore} from "vuex";


export default defineNuxtPlugin(nuxtApp => {
    nuxtApp.vueApp.use(store);
    return{
        provide:{
            store:useStore()
        }
    }
});
