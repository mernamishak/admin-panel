import productDialogs from "~/api/productDialogs";

export default defineNuxtPlugin(async (nuxtApp) => {
    return {
        provide: {
            editProductDialog: productDialogs
        }
    }
});
