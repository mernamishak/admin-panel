import vuetify from 'vite-plugin-vuetify'
import {createResolver} from '@nuxt/kit'

const {resolve} = createResolver(import.meta.url)

export default defineNuxtConfig({
    devtools: {enabled: true},
    css: [
        '~/assets/styles/main.scss',
        'vuetify/lib/styles/main.sass',
        '@mdi/font/css/materialdesignicons.min.css',

    ],
    build: {
        transpile: ['vuetify'],
    },

    vite: {
        define: {
            'process.env.DEBUG': false,
        },
        server:{
            proxy: {
                '/api': {
                    target: 'https://fakestoreapi.com',
                    changeOrigin: true,
                    secure: false,
                    ws: true,
                    rewrite: (path: string) => path.replace(/^\/api/, '')
                },
            },
            cors: false
        }
    },
    devServer:{
        proxy: 'http://localhost:3000'
    }
})
